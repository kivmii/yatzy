// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   Dice.java

import java.awt.*;

class Dice extends Canvas
{

    public Dice(int i)
    {
        diceNumber = i;
    }

    public int result()
    {
        return diceNumber;
    }

    public void paint(Graphics g)
    {
        g.drawRect(0, 0, 50, 50);
        if(diceNumber == 1)
            g.fillOval(20, 20, 10, 10);
        else
        if(diceNumber == 2)
        {
            g.fillOval(10, 10, 10, 10);
            g.fillOval(30, 30, 10, 10);
        } else
        if(diceNumber == 3)
        {
            g.fillOval(7, 7, 10, 10);
            g.fillOval(20, 20, 10, 10);
            g.fillOval(33, 33, 10, 10);
        } else
        if(diceNumber == 4)
        {
            g.fillOval(7, 7, 10, 10);
            g.fillOval(33, 7, 10, 10);
            g.fillOval(7, 33, 10, 10);
            g.fillOval(33, 33, 10, 10);
        } else
        if(diceNumber == 5)
        {
            g.fillOval(7, 7, 10, 10);
            g.fillOval(33, 7, 10, 10);
            g.fillOval(20, 20, 10, 10);
            g.fillOval(7, 33, 10, 10);
            g.fillOval(33, 33, 10, 10);
        } else
        if(diceNumber == 6)
        {
            g.fillOval(10, 7, 10, 10);
            g.fillOval(30, 7, 10, 10);
            g.fillOval(10, 20, 10, 10);
            g.fillOval(30, 20, 10, 10);
            g.fillOval(10, 33, 10, 10);
            g.fillOval(30, 33, 10, 10);
        }
    }

    public void renumber(int i)
    {
        diceNumber = i;
        repaint();
    }

    private int diceNumber;
}
