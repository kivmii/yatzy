// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   DiceElement.java


class DiceElement
{

    public DiceElement(int i, boolean flag, boolean flag1, boolean flag2, int j, boolean flag3, String s)
    {
        diceNumber = i;
        checked = flag;
        playerOne = flag1;
        firstThrow = flag2;
        turn = j;
        lastThrow = flag3;
        result = s;
    }

    boolean playerOne;
    int diceNumber;
    boolean checked;
    boolean firstThrow;
    int turn;
    boolean lastThrow;
    String result;
}
