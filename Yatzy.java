// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   yatzy.java

import java.awt.*;
import java.awt.event.*;
import java.util.*;

class Yatzy extends Frame
    implements AdjustmentListener, ActionListener, WindowListener
{

    public Yatzy()
    {
        setName("Yatzy");
        setLayout(null);
        color = new Color(64, 67, 134);
        setBackground(Color.lightGray);
        addWindowListener(this);
        panel1 = new Panel();
        panel1.setBounds(0, 50, 173, 519);
        panel1.setLayout(null);
        add(panel1);
        panel1.setVisible(false);
        panel2 = new Panel();
        panel2.setBounds(173, 50, 608, 519);
        panel2.setLayout(null);
        add(panel2);
        panel2.setVisible(false);
        randomNumber = new Random();
        disabledButtonInitValue = new Boolean(true);
        throwNumberLabel = new Label();
        throwNumberLabel.setFont(new Font("dialog", 1, 14));
        throwNumberLabel.setBounds(20, 50, 150, 26);
        panel1.add(throwNumberLabel);
        turnLabel = new Label();
        turnLabel.setFont(new Font("dialog", 1, 18));
        turnLabel.setBounds(42, 11, 72, 30);
        panel1.add(turnLabel);
        menuBar = new MenuBar();
        gameMenu = new Menu();
        gameMenu.setLabel("Game");
        menuBar.add(gameMenu);
        newGameMenuItem = new MenuItem();
        newGameMenuItem.setLabel("New game");
        newGameMenuItem.addActionListener(this);
        gameMenu.add(newGameMenuItem);
        exitMenuItem = new MenuItem();
        exitMenuItem.setLabel("Exit");
        exitMenuItem.addActionListener(this);
        gameMenu.add(exitMenuItem);
        setMenuBar(menuBar);
        throwButton = new Button();
        throwButton.setBounds(37, 464, 100, 26);
        throwButton.setLabel("Throw");
        panel1.add(throwButton);
        throwButton.addActionListener(this);
        throwButton.setEnabled(false);
        onesButton = new Button();
        onesButton.setBounds(22, 43, 100, 26);
        onesButton.setLabel("Ones");
        panel2.add(onesButton);
        onesButton.addActionListener(this);
        twosButton = new Button();
        twosButton.setBounds(22, 73, 100, 26);
        twosButton.setLabel("Twos");
        panel2.add(twosButton);
        twosButton.addActionListener(this);
        threesButton = new Button();
        threesButton.setBounds(22, 103, 100, 26);
        threesButton.setLabel("Threes");
        panel2.add(threesButton);
        threesButton.addActionListener(this);
        foursButton = new Button();
        foursButton.setBounds(22, 133, 100, 26);
        foursButton.setLabel("Fours");
        panel2.add(foursButton);
        foursButton.addActionListener(this);
        fivesButton = new Button();
        fivesButton.setBounds(22, 163, 100, 26);
        fivesButton.setLabel("Fives");
        panel2.add(fivesButton);
        fivesButton.addActionListener(this);
        sixesButton = new Button();
        sixesButton.setBounds(22, 193, 100, 26);
        sixesButton.setLabel("Sixes");
        panel2.add(sixesButton);
        sixesButton.addActionListener(this);
        bonusLabel = new Label();
        bonusLabel.setAlignment(1);
        bonusLabel.setText("Bonus");
        bonusLabel.setBounds(22, 223, 100, 26);
        panel2.add(bonusLabel);
        tripleButton = new Button();
        tripleButton.setBounds(22, 253, 100, 26);
        tripleButton.setLabel("Triple");
        panel2.add(tripleButton);
        tripleButton.addActionListener(this);
        fourOfAKindButton = new Button();
        fourOfAKindButton.setBounds(22, 283, 100, 26);
        fourOfAKindButton.setLabel("Four of a kind");
        panel2.add(fourOfAKindButton);
        fourOfAKindButton.addActionListener(this);
        smallStraightButton = new Button();
        smallStraightButton.setBounds(22, 313, 100, 26);
        smallStraightButton.setLabel("Small straight");
        panel2.add(smallStraightButton);
        smallStraightButton.addActionListener(this);
        largeStraightButton = new Button();
        largeStraightButton.setBounds(22, 343, 100, 26);
        largeStraightButton.setLabel("Large straight");
        panel2.add(largeStraightButton);
        largeStraightButton.addActionListener(this);
        fullHouseButton = new Button();
        fullHouseButton.setBounds(22, 373, 100, 26);
        fullHouseButton.setLabel("Full house");
        panel2.add(fullHouseButton);
        fullHouseButton.addActionListener(this);
        changeButton = new Button();
        changeButton.setBounds(22, 403, 100, 26);
        changeButton.setLabel("Change");
        panel2.add(changeButton);
        changeButton.addActionListener(this);
        yatzooButton = new Button();
        yatzooButton.setBounds(22, 433, 100, 26);
        yatzooButton.setLabel("Yatzoo");
        panel2.add(yatzooButton);
        yatzooButton.addActionListener(this);
        totalLabel = new Label();
        totalLabel.setAlignment(1);
        totalLabel.setText("Total");
        totalLabel.setBounds(22, 463, 100, 26);
        panel2.add(totalLabel);
        textFieldp1 = new TextField();
        textFieldp1.setBounds(138, 11, 71, 26);
        textFieldp1.setEditable(false);
        textFieldp1.setFont(new Font("dialog", 1, 12));
        panel2.add(textFieldp1);
        textFieldp2 = new TextField();
        textFieldp2.setBounds(211, 11, 71, 26);
        textFieldp2.setEditable(false);
        textFieldp2.setFont(new Font("dialog", 1, 12));
        panel2.add(textFieldp2);
        textFieldOnesp1 = new TextField();
        textFieldOnesp1.setBounds(138, 43, 71, 26);
        textFieldOnesp1.setEditable(false);
        panel2.add(textFieldOnesp1);
        textFieldOnesp2 = new TextField();
        textFieldOnesp2.setBounds(211, 43, 71, 26);
        textFieldOnesp2.setEditable(false);
        panel2.add(textFieldOnesp2);
        textFieldTwosp1 = new TextField();
        textFieldTwosp1.setBounds(138, 73, 71, 26);
        textFieldTwosp1.setEditable(false);
        panel2.add(textFieldTwosp1);
        textFieldTwosp2 = new TextField();
        textFieldTwosp2.setBounds(211, 73, 71, 26);
        textFieldTwosp2.setEditable(false);
        panel2.add(textFieldTwosp2);
        textFieldThreesp1 = new TextField();
        textFieldThreesp1.setBounds(138, 103, 71, 26);
        textFieldThreesp1.setEditable(false);
        panel2.add(textFieldThreesp1);
        textFieldThreesp2 = new TextField();
        textFieldThreesp2.setBounds(211, 103, 71, 26);
        textFieldThreesp2.setEditable(false);
        panel2.add(textFieldThreesp2);
        textFieldFoursp1 = new TextField();
        textFieldFoursp1.setBounds(138, 133, 71, 26);
        textFieldFoursp1.setEditable(false);
        panel2.add(textFieldFoursp1);
        textFieldFoursp2 = new TextField();
        textFieldFoursp2.setBounds(211, 133, 71, 26);
        textFieldFoursp2.setEditable(false);
        panel2.add(textFieldFoursp2);
        textFieldFivesp1 = new TextField();
        textFieldFivesp1.setBounds(138, 163, 71, 26);
        textFieldFivesp1.setEditable(false);
        panel2.add(textFieldFivesp1);
        textFieldFivesp2 = new TextField();
        textFieldFivesp2.setBounds(211, 163, 71, 26);
        textFieldFivesp2.setEditable(false);
        panel2.add(textFieldFivesp2);
        textFieldSixesp1 = new TextField();
        textFieldSixesp1.setBounds(138, 193, 71, 26);
        textFieldSixesp1.setEditable(false);
        panel2.add(textFieldSixesp1);
        textFieldSixesp2 = new TextField();
        textFieldSixesp2.setBounds(211, 193, 71, 26);
        textFieldSixesp2.setEditable(false);
        panel2.add(textFieldSixesp2);
        textFieldBonusp1 = new TextField();
        textFieldBonusp1.setBounds(138, 223, 71, 26);
        textFieldBonusp1.setEditable(false);
        panel2.add(textFieldBonusp1);
        textFieldBonusp2 = new TextField();
        textFieldBonusp2.setBounds(211, 223, 71, 26);
        textFieldBonusp2.setEditable(false);
        panel2.add(textFieldBonusp2);
        textFieldTriplep1 = new TextField();
        textFieldTriplep1.setBounds(138, 253, 71, 26);
        textFieldTriplep1.setEditable(false);
        panel2.add(textFieldTriplep1);
        textFieldTriplep2 = new TextField();
        textFieldTriplep2.setBounds(211, 253, 71, 26);
        textFieldTriplep2.setEditable(false);
        panel2.add(textFieldTriplep2);
        textFieldFourOfAKindp1 = new TextField();
        textFieldFourOfAKindp1.setBounds(138, 283, 71, 26);
        textFieldFourOfAKindp1.setEditable(false);
        panel2.add(textFieldFourOfAKindp1);
        textFieldFourOfAKindp2 = new TextField();
        textFieldFourOfAKindp2.setBounds(211, 283, 71, 26);
        textFieldFourOfAKindp2.setEditable(false);
        panel2.add(textFieldFourOfAKindp2);
        textFieldSmallStraightp1 = new TextField();
        textFieldSmallStraightp1.setBounds(138, 313, 71, 26);
        textFieldSmallStraightp1.setEditable(false);
        panel2.add(textFieldSmallStraightp1);
        textFieldSmallStraightp2 = new TextField();
        textFieldSmallStraightp2.setBounds(211, 313, 71, 26);
        textFieldSmallStraightp2.setEditable(false);
        panel2.add(textFieldSmallStraightp2);
        textFieldLargeStraightp1 = new TextField();
        textFieldLargeStraightp1.setBounds(138, 343, 71, 26);
        textFieldLargeStraightp1.setEditable(false);
        panel2.add(textFieldLargeStraightp1);
        textFieldLargeStraightp2 = new TextField();
        textFieldLargeStraightp2.setBounds(211, 343, 71, 26);
        textFieldLargeStraightp2.setEditable(false);
        panel2.add(textFieldLargeStraightp2);
        textFieldFullHousep1 = new TextField();
        textFieldFullHousep1.setBounds(138, 373, 71, 26);
        textFieldFullHousep1.setEditable(false);
        panel2.add(textFieldFullHousep1);
        textFieldFullHousep2 = new TextField();
        textFieldFullHousep2.setBounds(211, 373, 71, 26);
        textFieldFullHousep2.setEditable(false);
        panel2.add(textFieldFullHousep2);
        textFieldChangep1 = new TextField();
        textFieldChangep1.setBounds(138, 403, 71, 26);
        textFieldChangep1.setEditable(false);
        panel2.add(textFieldChangep1);
        textFieldChangep2 = new TextField();
        textFieldChangep2.setBounds(211, 403, 71, 26);
        textFieldChangep2.setEditable(false);
        panel2.add(textFieldChangep2);
        textFieldYatzoop1 = new TextField();
        textFieldYatzoop1.setBounds(138, 433, 71, 26);
        textFieldYatzoop1.setEditable(false);
        panel2.add(textFieldYatzoop1);
        textFieldYatzoop2 = new TextField();
        textFieldYatzoop2.setBounds(211, 433, 71, 26);
        textFieldYatzoop2.setEditable(false);
        panel2.add(textFieldYatzoop2);
        textFieldTotalp1 = new TextField();
        textFieldTotalp1.setBounds(138, 463, 71, 26);
        textFieldTotalp1.setFont(new Font("dialog", 1, 14));
        textFieldTotalp1.setEditable(false);
        panel2.add(textFieldTotalp1);
        textFieldTotalp2 = new TextField();
        textFieldTotalp2.setBounds(211, 463, 71, 26);
        textFieldTotalp2.setFont(new Font("dialog", 1, 14));
        textFieldTotalp2.setEditable(false);
        panel2.add(textFieldTotalp2);
        dice1 = new Dice(0);
        dice2 = new Dice(0);
        dice3 = new Dice(0);
        dice4 = new Dice(0);
        dice5 = new Dice(0);
        dice1.setBounds(44, 98, 51, 51);
        dice1.setBackground(Color.white);
        panel1.add(dice1);
        dice2.setBounds(44, 168, 51, 51);
        dice2.setBackground(Color.white);
        panel1.add(dice2);
        dice3.setBounds(44, 238, 51, 51);
        dice3.setBackground(Color.white);
        panel1.add(dice3);
        dice4.setBounds(44, 308, 51, 51);
        dice4.setBackground(Color.white);
        panel1.add(dice4);
        dice5.setBounds(44, 378, 51, 51);
        dice5.setBackground(Color.white);
        panel1.add(dice5);
        checkbox1 = new Checkbox();
        checkbox1.setBounds(7, 32, 71, 30);
        checkbox1.setLabel("Lock");
        checkbox2 = new Checkbox();
        checkbox2.setBounds(7, 102, 71, 30);
        checkbox2.setLabel("Lock");
        checkbox3 = new Checkbox();
        checkbox3.setBounds(7, 172, 71, 30);
        checkbox3.setLabel("Lock");
        checkbox4 = new Checkbox();
        checkbox4.setBounds(7, 242, 71, 30);
        checkbox4.setLabel("Lock");
        checkbox5 = new Checkbox();
        checkbox5.setBounds(7, 312, 71, 30);
        checkbox5.setLabel("Lock");
        checkboxPanel = new Panel();
        checkboxPanel.setLayout(null);
        checkboxPanel.setBounds(102, 95, 71, 358);
        checkboxPanel.add(checkbox1);
        checkboxPanel.add(checkbox2);
        checkboxPanel.add(checkbox3);
        checkboxPanel.add(checkbox4);
        checkboxPanel.add(checkbox5);
        panel1.add(checkboxPanel);
        namePlayersOKButton = new Button();
        namePlayersOKButton.setBounds(290, 250, 200, 52);
        namePlayersOKButton.setFont(new Font("dialog", 1, 16));
        namePlayersOKButton.setLabel("OK");
        namePlayersOKButton.addActionListener(this);
        p1Label = new Label();
        p1Label.setBounds(290, 160, 90, 30);
        p1Label.setFont(new Font("dialog", 1, 18));
        p1Label.setText("Player 1");
        p2Label = new Label();
        p2Label.setBounds(400, 160, 90, 30);
        p2Label.setFont(new Font("dialog", 1, 18));
        p2Label.setText("Player 2");
        p1TextField = new TextField();
        p1TextField.setBounds(290, 200, 90, 30);
        p1TextField.setFont(new Font("dialog", 1, 14));
        p1TextField.setEditable(true);
        p2TextField = new TextField();
        p2TextField.setBounds(400, 200, 90, 30);
        p2TextField.setFont(new Font("dialog", 1, 14));
        p2TextField.setEditable(true);
        namePlayersPanel = new Panel();
        namePlayersPanel.setLayout(null);
        namePlayersPanel.setBounds(0, 0, 781, 519);
        namePlayersPanel.add(p1Label);
        namePlayersPanel.add(p2Label);
        namePlayersPanel.add(namePlayersOKButton);
        namePlayersPanel.add(p1TextField);
        namePlayersPanel.add(p2TextField);
        add(namePlayersPanel);
        gameList = new Panel();
        gameList.setBounds(302, 11, 301, 479);
        gameList.setLayout(null);
        gameList.setBackground(Color.white);
        panel2.add(gameList);
        drawingSurface = new DrawingSurface(1, 600);
        drawingSurface.setBounds(4, 4, 270, 475);
        gameList.add(drawingSurface);
        gameListScrollbar = new Scrollbar(1, 0, 301, 0, 5670);
        gameListScrollbar.setBounds(274, 0, 27, 479);
        gameListScrollbar.setUnitIncrement(54);
        gameListScrollbar.addAdjustmentListener(this);
        gameList.add(gameListScrollbar);
    }

    public void actionPerformed(ActionEvent actionevent)
    {
        if(actionevent.getSource() == throwButton)
            throwDices();
        if(actionevent.getSource() == onesButton)
        {
            countOnes();
            changePlayer();
            playTurn();
        }
        if(actionevent.getSource() == twosButton)
        {
            countTwos();
            changePlayer();
            playTurn();
        }
        if(actionevent.getSource() == threesButton)
        {
            countThrees();
            changePlayer();
            playTurn();
        }
        if(actionevent.getSource() == foursButton)
        {
            countFours();
            changePlayer();
            playTurn();
        }
        if(actionevent.getSource() == fivesButton)
        {
            countFives();
            changePlayer();
            playTurn();
        }
        if(actionevent.getSource() == sixesButton)
        {
            countSixes();
            changePlayer();
            playTurn();
        }
        if(actionevent.getSource() == tripleButton)
        {
            countTriple();
            changePlayer();
            playTurn();
        }
        if(actionevent.getSource() == fourOfAKindButton)
        {
            countFourOfAKind();
            changePlayer();
            playTurn();
        }
        if(actionevent.getSource() == smallStraightButton)
        {
            countSmallStraight();
            changePlayer();
            playTurn();
        }
        if(actionevent.getSource() == largeStraightButton)
        {
            countLargeStraight();
            changePlayer();
            playTurn();
        }
        if(actionevent.getSource() == fullHouseButton)
        {
            countFullHouse();
            changePlayer();
            playTurn();
        }
        if(actionevent.getSource() == changeButton)
        {
            countChange();
            changePlayer();
            playTurn();
        }
        if(actionevent.getSource() == yatzooButton)
        {
            countYatzoo();
            changePlayer();
            playTurn();
        }
        if(actionevent.getSource() == newGameMenuItem)
            namePlayers();
        if(actionevent.getSource() == namePlayersOKButton)
            savePlayerNames();
        if(actionevent.getSource() == exitMenuItem)
            dispose();
    }

    public void adjustmentValueChanged(AdjustmentEvent adjustmentevent)
    {
        drawingSurface.paint(gameListScrollbar.getValue());
    }

    public void changePlayer()
    {
        playerOne = !playerOne;
        if(playerOne)
            turnOfPlayer++;
        setSBValue();
        drawingSurface.paint(gameListScrollbar.getValue());
    }

    public void countChange()
    {
        boolean flag = false;
        if(throwOfPlayer == 1)
            flag = true;
        int i = dice1.result();
        i += dice2.result();
        i += dice3.result();
        i += dice4.result();
        i += dice5.result();
        Integer integer = new Integer(i);
        if(playerOne)
        {
            textFieldChangep1.setText(integer.toString());
            changeButton.setEnabled(false);
            Boolean boolean1 = new Boolean(false);
            playerOneDisabledButtons.setElementAt(boolean1, 11);
        } else
        if(!playerOne)
        {
            textFieldChangep2.setText(integer.toString());
            changeButton.setEnabled(false);
            Boolean boolean2 = new Boolean(false);
            playerTwoDisabledButtons.setElementAt(boolean2, 11);
        }
        DiceElement diceelement = new DiceElement(dice1.result(), true, playerOne, flag, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice2.result(), true, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice3.result(), true, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice4.result(), true, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice5.result(), true, playerOne, false, turnOfPlayer, true, "Change " + i);
        drawingSurface.add(diceelement);
    }

    public void countFives()
    {
        boolean flag = false;
        if(throwOfPlayer == 1)
            flag = true;
        int i = 0;
        int j = 0;
        j = dice1.result();
        if(j == 5)
            i += j;
        j = 0;
        j = dice2.result();
        if(j == 5)
            i += j;
        j = 0;
        j = dice3.result();
        if(j == 5)
            i += j;
        j = 0;
        j = dice4.result();
        if(j == 5)
            i += j;
        j = 0;
        j = dice5.result();
        if(j == 5)
            i += j;
        Integer integer = new Integer(i);
        if(playerOne)
        {
            oneToSixIndexp1++;
            textFieldFivesp1.setText(integer.toString());
            fivesButton.setEnabled(false);
            Boolean boolean1 = new Boolean(false);
            playerOneDisabledButtons.setElementAt(boolean1, 4);
            if(oneToSixIndexp1 == 6)
                countOneToSix();
        } else
        if(!playerOne)
        {
            oneToSixIndexp2++;
            textFieldFivesp2.setText(integer.toString());
            fivesButton.setEnabled(false);
            Boolean boolean2 = new Boolean(false);
            playerTwoDisabledButtons.setElementAt(boolean2, 4);
            if(oneToSixIndexp2 == 6)
                countOneToSix();
        }
        DiceElement diceelement = new DiceElement(dice1.result(), dice1.result() == 5, playerOne, flag, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice2.result(), dice2.result() == 5, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice3.result(), dice3.result() == 5, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice4.result(), dice4.result() == 5, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice5.result(), dice5.result() == 5, playerOne, false, turnOfPlayer, true, "Fives " + i);
        drawingSurface.add(diceelement);
    }

    public void countFourOfAKind()
    {
        boolean flag = false;
        if(throwOfPlayer == 1)
            flag = true;
        int ai[] = new int[7];
        for(int i = 0; i <= 6; i++)
            ai[i] = 0;

        ai[dice1.result()]++;
        ai[dice2.result()]++;
        ai[dice3.result()]++;
        ai[dice4.result()]++;
        ai[dice5.result()]++;
        int j = 0;
        for(int k = 0; k <= 6; k++)
            if(ai[k] >= 4)
                j = k;

        int l = j * 4;
        Integer integer = new Integer(l);
        if(playerOne)
        {
            textFieldFourOfAKindp1.setText(integer.toString());
            fourOfAKindButton.setEnabled(false);
            Boolean boolean1 = new Boolean(false);
            playerOneDisabledButtons.setElementAt(boolean1, 7);
        } else
        if(!playerOne)
        {
            textFieldFourOfAKindp2.setText(integer.toString());
            fourOfAKindButton.setEnabled(false);
            Boolean boolean2 = new Boolean(false);
            playerTwoDisabledButtons.setElementAt(boolean2, 7);
        }
        DiceElement diceelement = new DiceElement(dice1.result(), dice1.result() == j, playerOne, flag, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice2.result(), dice2.result() == j, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice3.result(), dice3.result() == j, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice4.result(), dice4.result() == j, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice5.result(), dice5.result() == j, playerOne, false, turnOfPlayer, true, "Four of a kind " + l);
        drawingSurface.add(diceelement);
    }

    public void countFours()
    {
        boolean flag = false;
        if(throwOfPlayer == 1)
            flag = true;
        int i = 0;
        int j = 0;
        j = dice1.result();
        if(j == 4)
            i += j;
        j = 0;
        j = dice2.result();
        if(j == 4)
            i += j;
        j = 0;
        j = dice3.result();
        if(j == 4)
            i += j;
        j = 0;
        j = dice4.result();
        if(j == 4)
            i += j;
        j = 0;
        j = dice5.result();
        if(j == 4)
            i += j;
        Integer integer = new Integer(i);
        if(playerOne)
        {
            oneToSixIndexp1++;
            textFieldFoursp1.setText(integer.toString());
            foursButton.setEnabled(false);
            Boolean boolean1 = new Boolean(false);
            playerOneDisabledButtons.setElementAt(boolean1, 3);
            if(oneToSixIndexp1 == 6)
                countOneToSix();
        } else
        if(!playerOne)
        {
            oneToSixIndexp2++;
            textFieldFoursp2.setText(integer.toString());
            foursButton.setEnabled(false);
            Boolean boolean2 = new Boolean(false);
            playerTwoDisabledButtons.setElementAt(boolean2, 3);
            if(oneToSixIndexp2 == 6)
                countOneToSix();
        }
        DiceElement diceelement = new DiceElement(dice1.result(), dice1.result() == 4, playerOne, flag, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice2.result(), dice2.result() == 4, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice3.result(), dice3.result() == 4, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice4.result(), dice4.result() == 4, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice5.result(), dice5.result() == 4, playerOne, false, turnOfPlayer, true, "Fours " + i);
        drawingSurface.add(diceelement);
    }

    public void countFullHouse()
    {
        boolean flag = false;
        if(throwOfPlayer == 1)
            flag = true;
        int ai[] = new int[7];
        for(int i = 0; i <= 6; i++)
            ai[i] = 0;

        ai[dice1.result()]++;
        ai[dice2.result()]++;
        ai[dice3.result()]++;
        ai[dice4.result()]++;
        ai[dice5.result()]++;
        int j = 0;
        int k = 0;
        for(int l = 0; l <= 6; l++)
            if(ai[l] == 3)
                j = l;

        for(int i1 = 0; i1 <= 6; i1++)
            if(ai[i1] == 2)
                k = i1;

        int j1 = 0;
        if(k != 0 && j != 0)
            j1 = j * 3 + k * 2;
        Integer integer = new Integer(j1);
        if(playerOne)
        {
            textFieldFullHousep1.setText(integer.toString());
            fullHouseButton.setEnabled(false);
            Boolean boolean1 = new Boolean(false);
            playerOneDisabledButtons.setElementAt(boolean1, 10);
        } else
        if(!playerOne)
        {
            textFieldFullHousep2.setText(integer.toString());
            fullHouseButton.setEnabled(false);
            Boolean boolean2 = new Boolean(false);
            playerTwoDisabledButtons.setElementAt(boolean2, 10);
        }
        boolean flag1 = false;
        if(j1 != 0)
            flag1 = true;
        DiceElement diceelement = new DiceElement(dice1.result(), flag1, playerOne, flag, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice2.result(), flag1, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice3.result(), flag1, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice4.result(), flag1, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice5.result(), flag1, playerOne, false, turnOfPlayer, true, "Full house " + j1);
        drawingSurface.add(diceelement);
    }

    public void countLargeStraight()
    {
        boolean flag = false;
        if(throwOfPlayer == 1)
            flag = true;
        boolean aflag[] = new boolean[7];
        for(int i = 0; i <= 6; i++)
            aflag[i] = false;

        aflag[dice1.result()] = true;
        aflag[dice2.result()] = true;
        aflag[dice3.result()] = true;
        aflag[dice4.result()] = true;
        aflag[dice5.result()] = true;
        boolean flag1 = true;
        for(int j = 2; j <= 6; j++)
            if(!aflag[j])
                flag1 = false;

        byte byte0 = 0;
        if(flag1)
            byte0 = 20;
        Integer integer = new Integer(byte0);
        if(playerOne)
        {
            textFieldLargeStraightp1.setText(integer.toString());
            largeStraightButton.setEnabled(false);
            Boolean boolean1 = new Boolean(false);
            playerOneDisabledButtons.setElementAt(boolean1, 9);
        } else
        if(!playerOne)
        {
            textFieldLargeStraightp2.setText(integer.toString());
            largeStraightButton.setEnabled(false);
            Boolean boolean2 = new Boolean(false);
            playerTwoDisabledButtons.setElementAt(boolean2, 9);
        }
        boolean flag2 = false;
        if(byte0 != 0)
            flag2 = true;
        DiceElement diceelement = new DiceElement(dice1.result(), flag2, playerOne, flag, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice2.result(), flag2, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice3.result(), flag2, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice4.result(), flag2, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice5.result(), flag2, playerOne, false, turnOfPlayer, true, "Large straight " + byte0);
        drawingSurface.add(diceelement);
    }

    public void countOnes()
    {
        boolean flag = false;
        if(throwOfPlayer == 1)
            flag = true;
        int i = 0;
        int j = 0;
        j = dice1.result();
        if(j == 1)
            i += j;
        j = 0;
        j = dice2.result();
        if(j == 1)
            i += j;
        j = 0;
        j = dice3.result();
        if(j == 1)
            i += j;
        j = 0;
        j = dice4.result();
        if(j == 1)
            i += j;
        j = 0;
        j = dice5.result();
        if(j == 1)
            i += j;
        Integer integer = new Integer(i);
        if(playerOne)
        {
            oneToSixIndexp1++;
            textFieldOnesp1.setText(integer.toString());
            onesButton.setEnabled(false);
            Boolean boolean1 = new Boolean(false);
            playerOneDisabledButtons.setElementAt(boolean1, 0);
            if(oneToSixIndexp1 == 6)
                countOneToSix();
        } else
        if(!playerOne)
        {
            oneToSixIndexp2++;
            textFieldOnesp2.setText(integer.toString());
            onesButton.setEnabled(false);
            Boolean boolean2 = new Boolean(false);
            playerTwoDisabledButtons.setElementAt(boolean2, 0);
            if(oneToSixIndexp2 == 6)
                countOneToSix();
        }
        DiceElement diceelement = new DiceElement(dice1.result(), dice1.result() == 1, playerOne, flag, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice2.result(), dice2.result() == 1, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice3.result(), dice3.result() == 1, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice4.result(), dice4.result() == 1, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice5.result(), dice5.result() == 1, playerOne, false, turnOfPlayer, true, "Ones " + i);
        drawingSurface.add(diceelement);
    }

    public void countOneToSix()
    {
        if(playerOne)
        {
            String s = textFieldOnesp1.getText();
            Integer integer = new Integer(s);
            oneToSixp1 = integer.intValue();
            s = textFieldTwosp1.getText();
            Integer integer2 = new Integer(s);
            oneToSixp1 = oneToSixp1 + integer2.intValue();
            s = textFieldThreesp1.getText();
            Integer integer4 = new Integer(s);
            oneToSixp1 = oneToSixp1 + integer4.intValue();
            s = textFieldFoursp1.getText();
            Integer integer6 = new Integer(s);
            oneToSixp1 = oneToSixp1 + integer6.intValue();
            s = textFieldFivesp1.getText();
            Integer integer8 = new Integer(s);
            oneToSixp1 = oneToSixp1 + integer8.intValue();
            s = textFieldSixesp1.getText();
            Integer integer10 = new Integer(s);
            oneToSixp1 = oneToSixp1 + integer10.intValue();
            Integer integer12 = new Integer(oneToSixp1);
            if(integer12.intValue() >= 63)
                textFieldBonusp1.setText("20");
            else
                textFieldBonusp1.setText("0");
            oneToSixIndexp1 = 0;
            return;
        }
        String s1 = textFieldOnesp2.getText();
        Integer integer1 = new Integer(s1);
        oneToSixp2 = integer1.intValue();
        s1 = textFieldTwosp2.getText();
        Integer integer3 = new Integer(s1);
        oneToSixp2 = oneToSixp2 + integer3.intValue();
        s1 = textFieldThreesp2.getText();
        Integer integer5 = new Integer(s1);
        oneToSixp2 = oneToSixp2 + integer5.intValue();
        s1 = textFieldFoursp2.getText();
        Integer integer7 = new Integer(s1);
        oneToSixp2 = oneToSixp2 + integer7.intValue();
        s1 = textFieldFivesp2.getText();
        Integer integer9 = new Integer(s1);
        oneToSixp2 = oneToSixp2 + integer9.intValue();
        s1 = textFieldSixesp2.getText();
        Integer integer11 = new Integer(s1);
        oneToSixp2 = oneToSixp2 + integer11.intValue();
        Integer integer13 = new Integer(oneToSixp2);
        if(integer13.intValue() >= 63)
            textFieldBonusp2.setText("20");
        else
            textFieldBonusp2.setText("0");
        oneToSixIndexp2 = 0;
    }

    public void countSixes()
    {
        boolean flag = false;
        if(throwOfPlayer == 1)
            flag = true;
        int i = 0;
        int j = 0;
        j = dice1.result();
        if(j == 6)
            i += j;
        j = 0;
        j = dice2.result();
        if(j == 6)
            i += j;
        j = 0;
        j = dice3.result();
        if(j == 6)
            i += j;
        j = 0;
        j = dice4.result();
        if(j == 6)
            i += j;
        j = 0;
        j = dice5.result();
        if(j == 6)
            i += j;
        Integer integer = new Integer(i);
        if(playerOne)
        {
            oneToSixIndexp1++;
            textFieldSixesp1.setText(integer.toString());
            sixesButton.setEnabled(false);
            Boolean boolean1 = new Boolean(false);
            playerOneDisabledButtons.setElementAt(boolean1, 5);
            if(oneToSixIndexp1 == 6)
                countOneToSix();
        } else
        if(!playerOne)
        {
            oneToSixIndexp2++;
            textFieldSixesp2.setText(integer.toString());
            sixesButton.setEnabled(false);
            Boolean boolean2 = new Boolean(false);
            playerTwoDisabledButtons.setElementAt(boolean2, 5);
            if(oneToSixIndexp2 == 6)
                countOneToSix();
        }
        DiceElement diceelement = new DiceElement(dice1.result(), dice1.result() == 6, playerOne, flag, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice2.result(), dice2.result() == 6, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice3.result(), dice3.result() == 6, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice4.result(), dice4.result() == 6, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice5.result(), dice5.result() == 6, playerOne, false, turnOfPlayer, true, "Sixes " + i);
        drawingSurface.add(diceelement);
    }

    public void countSmallStraight()
    {
        boolean flag = false;
        if(throwOfPlayer == 1)
            flag = true;
        boolean aflag[] = new boolean[7];
        for(int i = 0; i <= 6; i++)
            aflag[i] = false;

        aflag[dice1.result()] = true;
        aflag[dice2.result()] = true;
        aflag[dice3.result()] = true;
        aflag[dice4.result()] = true;
        aflag[dice5.result()] = true;
        boolean flag1 = true;
        for(int j = 1; j <= 5; j++)
            if(!aflag[j])
                flag1 = false;

        byte byte0 = 0;
        if(flag1)
            byte0 = 15;
        Integer integer = new Integer(byte0);
        if(playerOne)
        {
            textFieldSmallStraightp1.setText(integer.toString());
            smallStraightButton.setEnabled(false);
            Boolean boolean1 = new Boolean(false);
            playerOneDisabledButtons.setElementAt(boolean1, 8);
        } else
        if(!playerOne)
        {
            textFieldSmallStraightp2.setText(integer.toString());
            smallStraightButton.setEnabled(false);
            Boolean boolean2 = new Boolean(false);
            playerTwoDisabledButtons.setElementAt(boolean2, 8);
        }
        boolean flag2 = false;
        if(byte0 != 0)
            flag2 = true;
        DiceElement diceelement = new DiceElement(dice1.result(), flag2, playerOne, flag, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice2.result(), flag2, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice3.result(), flag2, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice4.result(), flag2, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice5.result(), flag2, playerOne, false, turnOfPlayer, true, "Small straight " + byte0);
        drawingSurface.add(diceelement);
    }

    public void countThrees()
    {
        boolean flag = false;
        if(throwOfPlayer == 1)
            flag = true;
        int i = 0;
        int j = 0;
        j = dice1.result();
        if(j == 3)
            i += j;
        j = 0;
        j = dice2.result();
        if(j == 3)
            i += j;
        j = 0;
        j = dice3.result();
        if(j == 3)
            i += j;
        j = 0;
        j = dice4.result();
        if(j == 3)
            i += j;
        j = 0;
        j = dice5.result();
        if(j == 3)
            i += j;
        Integer integer = new Integer(i);
        if(playerOne)
        {
            oneToSixIndexp1++;
            textFieldThreesp1.setText(integer.toString());
            threesButton.setEnabled(false);
            Boolean boolean1 = new Boolean(false);
            playerOneDisabledButtons.setElementAt(boolean1, 2);
            if(oneToSixIndexp1 == 6)
                countOneToSix();
        } else
        if(!playerOne)
        {
            oneToSixIndexp2++;
            textFieldThreesp2.setText(integer.toString());
            threesButton.setEnabled(false);
            Boolean boolean2 = new Boolean(false);
            playerTwoDisabledButtons.setElementAt(boolean2, 2);
            if(oneToSixIndexp2 == 6)
                countOneToSix();
        }
        DiceElement diceelement = new DiceElement(dice1.result(), dice1.result() == 3, playerOne, flag, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice2.result(), dice2.result() == 3, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice3.result(), dice3.result() == 3, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice4.result(), dice4.result() == 3, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice5.result(), dice5.result() == 3, playerOne, false, turnOfPlayer, true, "Threes " + i);
        drawingSurface.add(diceelement);
    }

    public void countTotal()
    {
        throwButton.setEnabled(false);
        int i = oneToSixp1;
        String s = textFieldBonusp1.getText();
        Integer integer = new Integer(s);
        i += integer.intValue();
        s = textFieldTriplep1.getText();
        Integer integer1 = new Integer(s);
        i += integer1.intValue();
        s = textFieldFourOfAKindp1.getText();
        Integer integer2 = new Integer(s);
        i += integer2.intValue();
        s = textFieldSmallStraightp1.getText();
        Integer integer3 = new Integer(s);
        i += integer3.intValue();
        s = textFieldLargeStraightp1.getText();
        Integer integer4 = new Integer(s);
        i += integer4.intValue();
        s = textFieldFullHousep1.getText();
        Integer integer5 = new Integer(s);
        i += integer5.intValue();
        s = textFieldChangep1.getText();
        Integer integer6 = new Integer(s);
        i += integer6.intValue();
        s = textFieldYatzoop1.getText();
        Integer integer7 = new Integer(s);
        i += integer7.intValue();
        Integer integer8 = new Integer(i);
        textFieldTotalp1.setText(integer8.toString());
        int j = oneToSixp2;
        s = textFieldBonusp2.getText();
        Integer integer9 = new Integer(s);
        j += integer9.intValue();
        s = textFieldTriplep2.getText();
        Integer integer10 = new Integer(s);
        j += integer10.intValue();
        s = textFieldFourOfAKindp2.getText();
        Integer integer11 = new Integer(s);
        j += integer11.intValue();
        s = textFieldSmallStraightp2.getText();
        Integer integer12 = new Integer(s);
        j += integer12.intValue();
        s = textFieldLargeStraightp2.getText();
        Integer integer13 = new Integer(s);
        j += integer13.intValue();
        s = textFieldFullHousep2.getText();
        Integer integer14 = new Integer(s);
        j += integer14.intValue();
        s = textFieldChangep2.getText();
        Integer integer15 = new Integer(s);
        j += integer15.intValue();
        s = textFieldYatzoop2.getText();
        Integer integer16 = new Integer(s);
        j += integer16.intValue();
        Integer integer17 = new Integer(j);
        textFieldTotalp2.setText(integer17.toString());
    }

    public void countTriple()
    {
        boolean flag = false;
        if(throwOfPlayer == 1)
            flag = true;
        int ai[] = new int[7];
        for(int i = 0; i <= 6; i++)
            ai[i] = 0;

        ai[dice1.result()]++;
        ai[dice2.result()]++;
        ai[dice3.result()]++;
        ai[dice4.result()]++;
        ai[dice5.result()]++;
        int j = 0;
        for(int k = 0; k <= 6; k++)
            if(ai[k] >= 3)
                j = k;

        int l = j * 3;
        Integer integer = new Integer(l);
        if(playerOne)
        {
            textFieldTriplep1.setText(integer.toString());
            tripleButton.setEnabled(false);
            Boolean boolean1 = new Boolean(false);
            playerOneDisabledButtons.setElementAt(boolean1, 6);
        } else
        if(!playerOne)
        {
            textFieldTriplep2.setText(integer.toString());
            tripleButton.setEnabled(false);
            Boolean boolean2 = new Boolean(false);
            playerTwoDisabledButtons.setElementAt(boolean2, 6);
        }
        DiceElement diceelement = new DiceElement(dice1.result(), dice1.result() == j, playerOne, flag, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice2.result(), dice2.result() == j, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice3.result(), dice3.result() == j, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice4.result(), dice4.result() == j, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice5.result(), dice5.result() == j, playerOne, false, turnOfPlayer, true, "Triple " + l);
        drawingSurface.add(diceelement);
    }

    public void countTwos()
    {
        boolean flag = false;
        if(throwOfPlayer == 1)
            flag = true;
        int i = 0;
        int j = 0;
        j = dice1.result();
        if(j == 2)
            i += j;
        j = 0;
        j = dice2.result();
        if(j == 2)
            i += j;
        j = 0;
        j = dice3.result();
        if(j == 2)
            i += j;
        j = 0;
        j = dice4.result();
        if(j == 2)
            i += j;
        j = 0;
        j = dice5.result();
        if(j == 2)
            i += j;
        Integer integer = new Integer(i);
        if(playerOne)
        {
            oneToSixIndexp1++;
            textFieldTwosp1.setText(integer.toString());
            twosButton.setEnabled(false);
            Boolean boolean1 = new Boolean(false);
            playerOneDisabledButtons.setElementAt(boolean1, 1);
            if(oneToSixIndexp1 == 6)
                countOneToSix();
        } else
        if(!playerOne)
        {
            oneToSixIndexp2++;
            textFieldTwosp2.setText(integer.toString());
            twosButton.setEnabled(false);
            Boolean boolean2 = new Boolean(false);
            playerTwoDisabledButtons.setElementAt(boolean2, 1);
            if(oneToSixIndexp2 == 6)
                countOneToSix();
        }
        DiceElement diceelement = new DiceElement(dice1.result(), dice1.result() == 2, playerOne, flag, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice2.result(), dice2.result() == 2, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice3.result(), dice3.result() == 2, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice4.result(), dice4.result() == 2, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice5.result(), dice5.result() == 2, playerOne, false, turnOfPlayer, true, "Twos " + i);
        drawingSurface.add(diceelement);
    }

    public void countYatzoo()
    {
        boolean flag = false;
        if(throwOfPlayer == 1)
            flag = true;
        int ai[] = new int[7];
        for(int i = 0; i <= 6; i++)
            ai[i] = 0;

        ai[dice1.result()]++;
        ai[dice2.result()]++;
        ai[dice3.result()]++;
        ai[dice4.result()]++;
        ai[dice5.result()]++;
        boolean flag1 = false;
        for(int j = 1; j <= 6; j++)
            if(ai[j] == 5)
                flag1 = true;

        byte byte0 = 0;
        if(flag1)
            byte0 = 50;
        Integer integer = new Integer(byte0);
        if(playerOne)
        {
            textFieldYatzoop1.setText(integer.toString());
            yatzooButton.setEnabled(false);
            Boolean boolean1 = new Boolean(false);
            playerOneDisabledButtons.setElementAt(boolean1, 12);
        } else
        if(!playerOne)
        {
            textFieldYatzoop2.setText(integer.toString());
            yatzooButton.setEnabled(false);
            Boolean boolean2 = new Boolean(false);
            playerTwoDisabledButtons.setElementAt(boolean2, 12);
        }
        boolean flag2 = false;
        if(byte0 != 0)
            flag2 = true;
        DiceElement diceelement = new DiceElement(dice1.result(), flag2, playerOne, flag, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice2.result(), flag2, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice3.result(), flag2, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice4.result(), flag2, playerOne, false, turnOfPlayer, false, null);
        drawingSurface.add(diceelement);
        diceelement = new DiceElement(dice5.result(), flag2, playerOne, false, turnOfPlayer, true, "Yatzoo " + byte0);
        drawingSurface.add(diceelement);
    }

    public int drawLots()
    {
        int i = randomNumber.nextInt();
        i %= 10;
        if(i < 1 || i > 6)
            i = drawLots();
        return i;
    }

    public void namePlayers()
    {
        panel1.setVisible(false);
        panel2.setVisible(false);
        namePlayersPanel.setVisible(true);
    }

    public void playGame()
    {
        namePlayersPanel.setVisible(false);
        panel1.setVisible(true);
        panel2.setVisible(true);
        turn = 0;
        turnOfPlayer = 1;
        oneToSixIndexp1 = 0;
        oneToSixIndexp2 = 0;
        playerOne = true;
        textFieldOnesp1.setText("");
        textFieldTwosp1.setText("");
        textFieldThreesp1.setText("");
        textFieldFoursp1.setText("");
        textFieldFivesp1.setText("");
        textFieldSixesp1.setText("");
        textFieldBonusp1.setText("");
        textFieldTriplep1.setText("");
        textFieldFourOfAKindp1.setText("");
        textFieldSmallStraightp1.setText("");
        textFieldLargeStraightp1.setText("");
        textFieldFullHousep1.setText("");
        textFieldChangep1.setText("");
        textFieldYatzoop1.setText("");
        textFieldTotalp1.setText("");
        textFieldOnesp2.setText("");
        textFieldTwosp2.setText("");
        textFieldThreesp2.setText("");
        textFieldFoursp2.setText("");
        textFieldFivesp2.setText("");
        textFieldSixesp2.setText("");
        textFieldBonusp2.setText("");
        textFieldTriplep2.setText("");
        textFieldFourOfAKindp2.setText("");
        textFieldSmallStraightp2.setText("");
        textFieldLargeStraightp2.setText("");
        textFieldFullHousep2.setText("");
        textFieldChangep2.setText("");
        textFieldYatzoop2.setText("");
        textFieldTotalp2.setText("");
        if(playerOneDisabledButtons == null)
            playerOneDisabledButtons = new Vector();
        playerOneDisabledButtons.removeAllElements();
        playerOneDisabledButtons.addElement(disabledButtonInitValue);
        playerOneDisabledButtons.addElement(disabledButtonInitValue);
        playerOneDisabledButtons.addElement(disabledButtonInitValue);
        playerOneDisabledButtons.addElement(disabledButtonInitValue);
        playerOneDisabledButtons.addElement(disabledButtonInitValue);
        playerOneDisabledButtons.addElement(disabledButtonInitValue);
        playerOneDisabledButtons.addElement(disabledButtonInitValue);
        playerOneDisabledButtons.addElement(disabledButtonInitValue);
        playerOneDisabledButtons.addElement(disabledButtonInitValue);
        playerOneDisabledButtons.addElement(disabledButtonInitValue);
        playerOneDisabledButtons.addElement(disabledButtonInitValue);
        playerOneDisabledButtons.addElement(disabledButtonInitValue);
        playerOneDisabledButtons.addElement(disabledButtonInitValue);
        if(playerTwoDisabledButtons == null)
            playerTwoDisabledButtons = new Vector();
        playerTwoDisabledButtons.removeAllElements();
        playerTwoDisabledButtons.addElement(disabledButtonInitValue);
        playerTwoDisabledButtons.addElement(disabledButtonInitValue);
        playerTwoDisabledButtons.addElement(disabledButtonInitValue);
        playerTwoDisabledButtons.addElement(disabledButtonInitValue);
        playerTwoDisabledButtons.addElement(disabledButtonInitValue);
        playerTwoDisabledButtons.addElement(disabledButtonInitValue);
        playerTwoDisabledButtons.addElement(disabledButtonInitValue);
        playerTwoDisabledButtons.addElement(disabledButtonInitValue);
        playerTwoDisabledButtons.addElement(disabledButtonInitValue);
        playerTwoDisabledButtons.addElement(disabledButtonInitValue);
        playerTwoDisabledButtons.addElement(disabledButtonInitValue);
        playerTwoDisabledButtons.addElement(disabledButtonInitValue);
        playerTwoDisabledButtons.addElement(disabledButtonInitValue);
        drawingSurface.dices.removeAllElements();
        playTurn();
    }

    public void playTurn()
    {
        throwNumberLabel.setText("3 throws left");
        turn++;
        throwButton.setEnabled(true);
        onesButton.setEnabled(false);
        twosButton.setEnabled(false);
        threesButton.setEnabled(false);
        foursButton.setEnabled(false);
        fivesButton.setEnabled(false);
        sixesButton.setEnabled(false);
        tripleButton.setEnabled(false);
        fourOfAKindButton.setEnabled(false);
        smallStraightButton.setEnabled(false);
        largeStraightButton.setEnabled(false);
        fullHouseButton.setEnabled(false);
        changeButton.setEnabled(false);
        yatzooButton.setEnabled(false);
        checkbox1.setState(false);
        checkbox2.setState(false);
        checkbox3.setState(false);
        checkbox4.setState(false);
        checkbox5.setState(false);
        dice1.renumber(0);
        dice2.renumber(0);
        dice3.renumber(0);
        dice4.renumber(0);
        dice5.renumber(0);
        throwOfPlayer = 0;
        if(playerOne)
            turnLabel.setText(player1Name);
        else
            turnLabel.setText(player2Name);
        if(turn > 26)
        {
            turnLabel.setText("");
            throwNumberLabel.setText("");
            countTotal();
        }
    }

    public void savePlayerNames()
    {
        player1Name = p1TextField.getText();
        player2Name = p2TextField.getText();
        textFieldp1.setText(player1Name);
        textFieldp2.setText(player2Name);
        drawingSurface.namePlayers(player1Name, player2Name);
        playGame();
    }

    public void setButtonState(int i)
    {
        if(i == 1)
            onesButton.setEnabled(false);
        if(i == 2)
            twosButton.setEnabled(false);
        if(i == 3)
            threesButton.setEnabled(false);
        if(i == 4)
            foursButton.setEnabled(false);
        if(i == 5)
            fivesButton.setEnabled(false);
        if(i == 6)
            sixesButton.setEnabled(false);
        if(i == 7)
            tripleButton.setEnabled(false);
        if(i == 8)
            fourOfAKindButton.setEnabled(false);
        if(i == 9)
            smallStraightButton.setEnabled(false);
        if(i == 10)
            largeStraightButton.setEnabled(false);
        if(i == 11)
            fullHouseButton.setEnabled(false);
        if(i == 12)
            changeButton.setEnabled(false);
        if(i == 13)
            yatzooButton.setEnabled(false);
    }

    public void setSBValue()
    {
        gameListScrollbar.setValue(drawingSurface.getSBValue());
    }

    public void throwDices()
    {
        throwOfPlayer++;
        if(throwOfPlayer == 1)
        {
            throwNumberLabel.setText("2 throws left");
            onesButton.setEnabled(true);
            twosButton.setEnabled(true);
            threesButton.setEnabled(true);
            foursButton.setEnabled(true);
            fivesButton.setEnabled(true);
            sixesButton.setEnabled(true);
            tripleButton.setEnabled(true);
            fourOfAKindButton.setEnabled(true);
            smallStraightButton.setEnabled(true);
            largeStraightButton.setEnabled(true);
            fullHouseButton.setEnabled(true);
            changeButton.setEnabled(true);
            yatzooButton.setEnabled(true);
            int i = 1;
            if(playerOne)
            {
                Enumeration enumeration = playerOneDisabledButtons.elements();
                do
                {
                    Boolean boolean1 = (Boolean)enumeration.nextElement();
                    if(!boolean1.booleanValue())
                        setButtonState(i);
                    i++;
                } while(enumeration.hasMoreElements());
            } else
            {
                Enumeration enumeration1 = playerTwoDisabledButtons.elements();
                do
                {
                    Boolean boolean2 = (Boolean)enumeration1.nextElement();
                    if(!boolean2.booleanValue())
                        setButtonState(i);
                    i++;
                } while(enumeration1.hasMoreElements());
            }
        }
        if(throwOfPlayer == 2)
        {
            DiceElement diceelement = new DiceElement(dice1.result(), checkbox1.getState(), playerOne, true, turnOfPlayer, false, null);
            drawingSurface.add(diceelement);
            diceelement = new DiceElement(dice2.result(), checkbox2.getState(), playerOne, false, turnOfPlayer, false, null);
            drawingSurface.add(diceelement);
            diceelement = new DiceElement(dice3.result(), checkbox3.getState(), playerOne, false, turnOfPlayer, false, null);
            drawingSurface.add(diceelement);
            diceelement = new DiceElement(dice4.result(), checkbox4.getState(), playerOne, false, turnOfPlayer, false, null);
            drawingSurface.add(diceelement);
            diceelement = new DiceElement(dice5.result(), checkbox5.getState(), playerOne, false, turnOfPlayer, false, null);
            drawingSurface.add(diceelement);
            throwNumberLabel.setText("1 throw left");
        }
        if(throwOfPlayer == 3)
        {
            throwButton.setEnabled(false);
            throwOfPlayer = 0;
            DiceElement diceelement1 = new DiceElement(dice1.result(), checkbox1.getState(), playerOne, false, turnOfPlayer, false, null);
            drawingSurface.add(diceelement1);
            diceelement1 = new DiceElement(dice2.result(), checkbox2.getState(), playerOne, false, turnOfPlayer, false, null);
            drawingSurface.add(diceelement1);
            diceelement1 = new DiceElement(dice3.result(), checkbox3.getState(), playerOne, false, turnOfPlayer, false, null);
            drawingSurface.add(diceelement1);
            diceelement1 = new DiceElement(dice4.result(), checkbox4.getState(), playerOne, false, turnOfPlayer, false, null);
            drawingSurface.add(diceelement1);
            diceelement1 = new DiceElement(dice5.result(), checkbox5.getState(), playerOne, false, turnOfPlayer, false, null);
            drawingSurface.add(diceelement1);
            throwNumberLabel.setText("");
        }
        if(!checkbox1.getState())
            dice1.renumber(drawLots());
        if(!checkbox2.getState())
            dice2.renumber(drawLots());
        if(!checkbox3.getState())
            dice3.renumber(drawLots());
        if(!checkbox4.getState())
            dice4.renumber(drawLots());
        if(!checkbox5.getState())
            dice5.renumber(drawLots());
    }

    public void windowActivated(WindowEvent windowevent)
    {
    }

    public void windowClosed(WindowEvent windowevent)
    {
    }

    public void windowClosing(WindowEvent windowevent)
    {
        if(windowevent.getSource() == this)
            dispose();
    }

    public void windowDeactivated(WindowEvent windowevent)
    {
    }

    public void windowDeiconified(WindowEvent windowevent)
    {
    }

    public void windowIconified(WindowEvent windowevent)
    {
    }

    public void windowOpened(WindowEvent windowevent)
    {
    }

    private int turn;
    private int turnOfPlayer;
    private int oneToSixIndexp1;
    private int oneToSixIndexp2;
    private Color color;
    private int oneToSixp1;
    private int oneToSixp2;
    private boolean playerOne;
    private String player1Name;
    private String player2Name;
    Boolean disabledButtonInitValue;
    private int throwOfPlayer;
    private Label throwNumberLabel;
    private Panel panel1;
    private Panel panel2;
    private Label turnLabel;
    private Vector playerOneDisabledButtons;
    private Vector playerTwoDisabledButtons;
    private Button throwButton;
    private Button onesButton;
    private Button twosButton;
    private Button threesButton;
    private Button foursButton;
    private Button fivesButton;
    private Button sixesButton;
    private Label bonusLabel;
    private Button tripleButton;
    private Button fourOfAKindButton;
    private Button smallStraightButton;
    private Button largeStraightButton;
    private Button fullHouseButton;
    private Button changeButton;
    private Button yatzooButton;
    private Label totalLabel;
    private MenuBar menuBar;
    private Menu gameMenu;
    private MenuItem newGameMenuItem;
    private MenuItem exitMenuItem;
    private TextField textFieldp1;
    private TextField textFieldp2;
    private TextField textFieldOnesp1;
    private TextField textFieldTwosp1;
    private TextField textFieldThreesp1;
    private TextField textFieldFoursp1;
    private TextField textFieldFivesp1;
    private TextField textFieldSixesp1;
    private TextField textFieldBonusp1;
    private TextField textFieldTriplep1;
    private TextField textFieldFourOfAKindp1;
    private TextField textFieldSmallStraightp1;
    private TextField textFieldLargeStraightp1;
    private TextField textFieldFullHousep1;
    private TextField textFieldChangep1;
    private TextField textFieldYatzoop1;
    private TextField textFieldTotalp1;
    private TextField textFieldOnesp2;
    private TextField textFieldTwosp2;
    private TextField textFieldThreesp2;
    private TextField textFieldFoursp2;
    private TextField textFieldFivesp2;
    private TextField textFieldSixesp2;
    private TextField textFieldBonusp2;
    private TextField textFieldTriplep2;
    private TextField textFieldFourOfAKindp2;
    private TextField textFieldSmallStraightp2;
    private TextField textFieldLargeStraightp2;
    private TextField textFieldFullHousep2;
    private TextField textFieldChangep2;
    private TextField textFieldYatzoop2;
    private TextField textFieldTotalp2;
    private Dice dice1;
    private Dice dice2;
    private Dice dice3;
    private Dice dice4;
    private Dice dice5;
    private Random randomNumber;
    private Panel checkboxPanel;
    private Checkbox checkbox1;
    private Checkbox checkbox2;
    private Checkbox checkbox3;
    private Checkbox checkbox4;
    private Checkbox checkbox5;
    private Label p1Label;
    private Label p2Label;
    private Panel namePlayersPanel;
    private Button namePlayersOKButton;
    private TextField p1TextField;
    private TextField p2TextField;
    private DrawingSurface drawingSurface;
    private Panel gameList;
    private Scrollbar gameListScrollbar;
}
