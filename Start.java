// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   Start.java

import java.applet.Applet;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventObject;

public class Start extends Applet
    implements ActionListener
{

    public void actionPerformed(ActionEvent actionevent)
    {
        if(actionevent.getSource() == getButton1())
            conn0(actionevent);
    }

    private void conn0(ActionEvent actionevent)
    {
        try
        {
            getYatzy1().show();
            return;
        }
        catch(Throwable throwable)
        {
            handleException(throwable);
        }
    }

    public String getAppletInfo()
    {
        return "StartButton created using VisualAge for Java.";
    }

    private Button getButton1()
    {
        if(ivjButton1 == null)
            try
            {
                ivjButton1 = new Button();
                ivjButton1.setName("Button1");
                ivjButton1.setBounds(27, 40, 80, 80);
                ivjButton1.setLabel("Yatzoo");
            }
            catch(Throwable throwable)
            {
                handleException(throwable);
            }
        return ivjButton1;
    }

    private Yatzy getYatzy1()
    {
        if(ivjYatzy1 == null)
            try
            {
                ivjYatzy1 = new Yatzy();
                ivjYatzy1.setName("Yatzy1");
                ivjYatzy1.setBounds(19, 83, 790, 600);
            }
            catch(Throwable throwable)
            {
                handleException(throwable);
            }
        return ivjYatzy1;
    }

    private void handleException(Throwable throwable)
    {
    }

    public void init()
    {
        super.init();
        try
        {
            setName("Start");
            setLayout(null);
            setSize(144, 163);
            add(getButton1(), getButton1().getName());
            initConnections();
            return;
        }
        catch(Throwable throwable)
        {
            handleException(throwable);
        }
    }

    private void initConnections()
    {
        getButton1().addActionListener(this);
    }

    public Start()
    {
    }

    private Button ivjButton1;
    private Yatzy ivjYatzy1;
}
