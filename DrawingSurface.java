// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   DrawingSurface.java

import java.awt.*;
import java.util.Enumeration;
import java.util.Vector;

class DrawingSurface extends Canvas
{

    public void paintOneDice(boolean flag)
    {
        int i = xMove * 54;
        int j = yMove * 54;
        g = getGraphics();
        if(flag)
        {
            g.setColor(Color.lightGray);
            g.fillRect(i + 0, j + 0, 50, 50);
            g.setColor(Color.black);
        }
        g.drawRect(i + 0, j + 0, 50, 50);
        g.fillOval(i + 20, j + 20, 10, 10);
        xMove++;
        if(xMove == 5)
        {
            yMove++;
            xMove = 0;
        }
    }

    public void add(DiceElement diceelement)
    {
        dices.addElement(diceelement);
    }

    public void paintThreeDice(boolean flag)
    {
        int i = xMove * 54;
        int j = yMove * 54;
        g = getGraphics();
        if(flag)
        {
            g.setColor(Color.lightGray);
            g.fillRect(i + 0, j + 0, 50, 50);
            g.setColor(Color.black);
        }
        g.drawRect(i + 0, j + 0, 50, 50);
        g.fillOval(i + 7, j + 7, 10, 10);
        g.fillOval(i + 20, j + 20, 10, 10);
        g.fillOval(i + 33, j + 33, 10, 10);
        xMove++;
        if(xMove == 5)
        {
            yMove++;
            xMove = 0;
        }
    }

    public void paintPlayer(int i)
    {
        g = getGraphics();
        g.setFont(new Font("dialog", 1, 12));
        g.drawString(currentPlayer + ": Throw " + i, 3, yMove * 54 + 49);
        yMove++;
    }

    public void paintTwoDice(boolean flag)
    {
        int i = xMove * 54;
        int j = yMove * 54;
        g = getGraphics();
        if(flag)
        {
            g.setColor(Color.lightGray);
            g.fillRect(i + 0, j + 0, 50, 50);
            g.setColor(Color.black);
        }
        g.drawRect(i + 0, j + 0, 50, 50);
        g.fillOval(i + 10, j + 10, 10, 10);
        g.fillOval(i + 30, j + 30, 10, 10);
        xMove++;
        if(xMove == 5)
        {
            yMove++;
            xMove = 0;
        }
    }

    public void paintSixDice(boolean flag)
    {
        int i = xMove * 54;
        int j = yMove * 54;
        g = getGraphics();
        if(flag)
        {
            g.setColor(Color.lightGray);
            g.fillRect(i + 0, j + 0, 50, 50);
            g.setColor(Color.black);
        }
        g.drawRect(i + 0, j + 0, 50, 50);
        g.fillOval(i + 10, j + 7, 10, 10);
        g.fillOval(i + 30, j + 7, 10, 10);
        g.fillOval(i + 10, j + 20, 10, 10);
        g.fillOval(i + 30, j + 20, 10, 10);
        g.fillOval(i + 10, j + 33, 10, 10);
        g.fillOval(i + 30, j + 33, 10, 10);
        xMove++;
        if(xMove == 5)
        {
            yMove++;
            xMove = 0;
        }
    }

    public int getSBValue()
    {
        int i = 0;
        int j = 0;
        Enumeration enumeration = dices.elements();
        do
        {
            DiceElement diceelement = (DiceElement)enumeration.nextElement();
            if(++j == 5)
            {
                i++;
                j = 0;
            }
        } while(enumeration.hasMoreElements());
        if(i > 7)
            return i * 54 - 216;
        else
            return 0;
    }

    public void namePlayers(String s, String s1)
    {
        player1 = s;
        player2 = s1;
        currentPlayer = player1;
    }

    public void paintFourDice(boolean flag)
    {
        int i = xMove * 54;
        int j = yMove * 54;
        g = getGraphics();
        if(flag)
        {
            g.setColor(Color.lightGray);
            g.fillRect(i + 0, j + 0, 50, 50);
            g.setColor(Color.black);
        }
        g.drawRect(i + 0, j + 0, 50, 50);
        g.fillOval(i + 7, j + 7, 10, 10);
        g.fillOval(i + 33, j + 7, 10, 10);
        g.fillOval(i + 7, j + 33, 10, 10);
        g.fillOval(i + 33, j + 33, 10, 10);
        xMove++;
        if(xMove == 5)
        {
            yMove++;
            xMove = 0;
        }
    }

    public void paint(int i)
    {
        xMove = 0;
        yMove = i / 54;
        yMove = -yMove;
        byte byte0 = 4;
        g = getGraphics();
        g.setColor(Color.white);
        g.fillRect(0, 0, 300, 600);
        Enumeration enumeration = dices.elements();
        do
        {
            DiceElement diceelement = (DiceElement)enumeration.nextElement();
            if(yMove >= 0)
            {
                if(diceelement.firstThrow)
                {
                    if(diceelement.playerOne)
                        currentPlayer = player1;
                    else
                        currentPlayer = player2;
                    paintPlayer(diceelement.turn);
                }
                if(diceelement.diceNumber == 1)
                    paintOneDice(diceelement.checked);
                if(diceelement.diceNumber == 2)
                    paintTwoDice(diceelement.checked);
                if(diceelement.diceNumber == 3)
                    paintThreeDice(diceelement.checked);
                if(diceelement.diceNumber == 4)
                    paintFourDice(diceelement.checked);
                if(diceelement.diceNumber == 5)
                    paintFiveDice(diceelement.checked);
                if(diceelement.diceNumber == 6)
                    paintSixDice(diceelement.checked);
                if(diceelement.lastThrow)
                    paintResult(diceelement.result);
            } else
            {
                xMove++;
                if(xMove == 5)
                {
                    yMove++;
                    xMove = 0;
                }
            }
        } while(enumeration.hasMoreElements() && yMove < 10);
    }

    public void paintFiveDice(boolean flag)
    {
        int i = xMove * 54;
        int j = yMove * 54;
        g = getGraphics();
        if(flag)
        {
            g.setColor(Color.lightGray);
            g.fillRect(i + 0, j + 0, 50, 50);
            g.setColor(Color.black);
        }
        g.drawRect(i + 0, j + 0, 50, 50);
        g.fillOval(i + 7, j + 7, 10, 10);
        g.fillOval(i + 33, j + 7, 10, 10);
        g.fillOval(i + 20, j + 20, 10, 10);
        g.fillOval(i + 7, j + 33, 10, 10);
        g.fillOval(i + 33, j + 33, 10, 10);
        xMove++;
        if(xMove == 5)
        {
            yMove++;
            xMove = 0;
        }
    }

    public void paintResult(String s)
    {
        g = getGraphics();
        g.setFont(new Font("dialog", 1, 12));
        g.drawString(s, 3, yMove * 54 + 18);
    }

    public DrawingSurface(int i, int j)
    {
        g = null;
        dices = null;
        xMove = 0;
        yMove = 0;
        dices = new Vector();
        font = new Font("dialog", 1, 18);
    }

    private int xMove;
    private int yMove;
    private Graphics g;
    public Vector dices;
    private String player1;
    private String player2;
    private String currentPlayer;
    private Font font;
}
